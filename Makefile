
CC = rvex-gcc
FILES = fail.c random.c slowfloat.c testCases.c testLoops.c testsoftfloat.c writeHex.c

#if you wish to run the tests on a hardware implementation instead the simulator, you will need
#to add a platform library that implements at least the write syscall to be able to see the output.
PLATFORM_LIB = 

all: testsoftfloat testhardfloat

testsoftfloat: $(FILES)
	$(CC) -o $@ -mruntime=newlib $^ $(PLATFORM_LIB) -D__HAS_PREDICATES

testhardfloat: $(FILES)
	$(CC) -mcore=rvex_fp -o $@ -mruntime=newlib hardfloat.c $^ $(PLATFORM_LIB) -D__HAS_PREDICATES -DHARDFLOAT

clean:
	rm -rf testsoftfloat testhardfloat
