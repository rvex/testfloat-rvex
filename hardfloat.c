#include <math.h>


float hw_int32_to_float32(int a)
{
	return (float)a;
}

double hw_int32_to_float64(int a)
{
	return (double)a;
}

float hw_int64_to_float32(long long a)
{
	return (float)a;
}

double hw_int64_to_float64(long long a)
{
	return (double)a;
}

int hw_float32_to_int32(float a)
{
	return (int)a;
}

long long hw_float32_to_int64(float a)
{
	return (long long)a;
}

float hw_float32_add(float a, float b)
{
	return a+b;
}

float hw_float32_sub(float a, float b)
{
	return a - b;
}

float hw_float32_mul(float a, float b)
{
	return a*b;
}

float hw_float32_div(float a, float b)
{
	return a/b;
}

float hw_float32_sqrt(float a)
{
	return sqrt(a);
}

int hw_float32_eq(float a, float b)
{
	if (a == b)
		return 1;
	else
		return 0;
}

int hw_float32_le(float a, float b)
{
	if (a <= b)
		return 1;
	else
		return 0;
}

int hw_float32_lt(float a, float b)
{
	if (a < b)
		return 1;
	else
		return 0;
}

int hw_float64_to_int32(double a)
{
	return (int)a;
}

long long hw_float64_to_int64(double a)
{
	return (long long)a;
}

float hw_float64_to_float32(double a)
{
	return (float)a;
}

double hw_float64_add(double a, double b)
{
	return a+b;
}

double hw_float64_sub(double a, double b)
{
	return a-b;
}

double hw_float64_mul(double a, double b)
{
	return a*b;
}

double hw_float64_div(double a, double b)
{
	return a/b;
}


double hw_float64_sqrt(double a)
{
	return sqrt(a);
}


int hw_float64_eq(double a, double b)
{
	if (a == b)
		return 1;
	else
		return 0;
}

int hw_float64_le(double a, double b)
{
	if (a <= b)
		return 1;
	else
		return 0;
}

int hw_float64_lt(double a, double b)
{
	if (a < b)
		return 1;
	else
		return 0;
}
