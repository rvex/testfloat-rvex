#TestFloat-rVEX
Floating point test suite for rVEX
Based on STMicroelectronics' libgcc test suite, in turn based on TestFloat by John Hauser (see http://HTTP.CS.Berkeley.EDU/~jhauser/arithmetic/TestFloat.html)

#Building
To build, make sure your rVEX toolchain in in your PATH and type make.
This will build 2 programs, testsoftfloat and testhardfloat.
testsoftfloat will test the software library that emulates floating point operations (included in rVEX libgcc).
testhardfloat will test the operations supported by the hardware floating point unit.

#Running the tests
After building, type 'simrvex -e test[hard/soft]float -- -all -tininessafter -nearesteven' to test on the simulator.
To test on a hardware platform, you will need to add a platform library that can implement the newlib syscalls (at least write, to output to UART for example). Add it in the Makefile, build the file, download it to the board and run it.
